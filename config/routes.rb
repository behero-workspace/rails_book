Rails.application.routes.draw do
  root to: 'static_pages#home'
  post '/users/sign_in' => 'sessions#create'
  get '/users/sign_in' => 'sessions#new', as: :sign_in

  post '/users/sign_up' => 'users#create'
  get '/users/sign_up' => 'users#new', as: :sign_up

  delete '/users/sign_out' => 'sessions#destroy', as: :sign_out

  get 'active_account/:token' => 'users#active_account', as: :active_account
  resources :users, only: [:show]
end
