class UserMailer < ApplicationMailer
  def account_activation(user)
    @user = user

    mail to: @user.email, subject: 'Please active your account!'
  end
end
