class User < ApplicationRecord
  has_secure_password

  # callback
  before_save { self.email.downcase! }
  before_create :generate_activation_token

  attr_accessor :activation_token

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  # Validation
  validates :name, presence: true
  validates :email, uniqueness: true, format: { with: VALID_EMAIL_REGEX, message: 'The email value is invalid' }

  def authenticated?(token)
    return false if activation_digest.nil?
    BCrypt::Password.new(activation_digest).is_password?(token)
  end

  def active_account!
    update_attributes activated: true, activated_at: Time.now, activation_digest: nil
  end

  private

  def generate_activation_token
    self.activation_token = SecureRandom.base64(20)
    self.activation_digest = BCrypt::Password.create(self.activation_token)
  end
end
