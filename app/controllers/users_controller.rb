class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new user_params

    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:success] = 'You are signed up successfully'
      redirect_to root_path
    else
      flash.now[:warning] = 'Something went wrong'
      render :new
    end
  end

  def active_account
    @user = User.find_by email: params[:email]

    if @user&.authenticated?(params[:token]) && @user&.active_account!
      flash[:success] = 'You are activated successfully'
    else
      flash[:alert] = 'Active link is invalid!!!'
    end
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit :name, :email, :password, :password_confirmation
  end
end
