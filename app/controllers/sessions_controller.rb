class SessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.find_by email: params[:user][:email]

    if @user&.authenticate(params[:user][:password])
      if @user.activated?
        login(@user)
        flash[:success] = 'You are signed in successfully!'
      else
        flash[:alert] = 'Your account is not activated. Please active it first and sign in again!!!'
      end
      redirect_to root_path
    else
      @user = User.new user_params
      flash.now[:warning] = 'Wrong email or password. please try again!'
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:sucess] = 'You are signed out successfully'
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit :email, :password
  end
end
